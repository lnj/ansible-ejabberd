xmpp                     IN A       xxx.xxx.xxx.xxx
xmpp                     IN AAAA    ::

echo                     IN CNAME   xmpp
muc                      IN CNAME   xmpp
proxy                    IN CNAME   xmpp
pubsub                   IN CNAME   xmpp
share                    IN CNAME   xmpp
vcards                   IN CNAME   xmpp

_xmpp-client._tcp        IN SRV     5 0 5222 xmpp
_xmpp-client._tcp        IN SRV     5 0 443 xmpp
_xmpps-client._tcp       IN SRV     5 0 5223 xmpp
_xmpps-client._tcp       IN SRV     5 0 443 xmpp
_xmpp-server._tcp        IN SRV     5 0 5269 xmpp
_xmpps-server._tcp       IN SRV     5 0 5270 xmpp
_xmppconnect             IN TXT     "_xmpp-client-xbosh=https://xmpp.@HOST@/bosh"

